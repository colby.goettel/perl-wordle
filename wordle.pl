#!/usr/bin/env perl

use strict;
use warnings;

use Term::ANSIColor;

my $count = 0;
my $DEBUG = 1;
my @guesses;

# Read input file into @word_list
my $file = 'words.csv';
my @word_list;

open(my $fh, '<', $file) or die "Can't read $file: $!\n";

while (my $line = <$fh>)
{
  chomp $line;
  push @word_list, $line;
}

close $fh;

# Create list of allowable words for user to guess
# awk -v RS="[ .,?!]|\n" 'length($0)==5 {print $0}' /usr/share/dict/words > five-letter-words.txt
$file = 'five-letter-words.txt';
my @allowable_words;

open(my $fh, '<', $file) or die "Can't read $file: $!\n";

while (my $line = <$fh>)
{
  chomp $line;
  push @allowable_words, $line;
}

close $fh;

# Pick a random word from word_list
my $random_number = int(rand(scalar(@word_list) - 1));
my $word = $word_list[$random_number];

# Run game until user guesses word
while ( $guesses[-1] ne $word and $count < 6 )
{
  print_info();

  # Read user input
  push @guesses, read_input();

  print_info();

  $count++;
}

print "\n";

# Check guess against $word and print colors for known values.
# Options: <none>
# Returns: <none>
sub check_guess
{
  foreach my $char (split //, $guesses[-1])
  {
    # If letter is in word and is in correct place, print it green
    if ( index($guesses[-1], $char) == 0 )
    {
      print color("green"), $char, color("reset");
    }
    # If letter is in word but not in correct place, print it yellow
    elsif ( index($guesses[-1], $char) != -1 )
    {
      print color("yellow"), $char, color("reset");
    }
    # Print letter (default color works fine)
    print "$char";
  }

  print "\n";
}

# Clears the screen and prints the available info. This is a messy hack, but for a quick program I don't care.
# Options: <none>
# Returns: <none>
sub print_info
{
  system("clear");
  print "Wordle clone\n\n";
  print "word:\t$word\n" if $DEBUG;
  print "guess:\t$guesses[-1]\n" if $DEBUG;
  foreach ( @guesses )
  {
    print "$_\n";
  }
}

# Reads user input
# Options: <none>
# Returns: (string) user's word guess
sub read_input
{
  my $_guess = "";

  # Clean input: must be in word_list
  while ()
  {
    $_guess = <STDIN>;
    chomp $_guess;

    if ( grep(/^$_guess$/, @word_list) )
    {
      return $_guess;
    }
    else
    {
      print color("red"), "$_guess\n", color("reset");
    }
  }
}
